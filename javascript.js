window.onload = function () {

	//Наименования классов
	const CHECK = "checkbox";
	const UNCHECK = "uncheckbox";

	let id = 0;

	var mass = [];
	if (localStorage.getItem('do')!=undefined){
		mass = JSON.parse(localStorage.getItem('do'));
		output();
	}

	document.getElementById('add').onclick = function (){
		var deal = document.getElementById('input').value; 
		if (deal.length === 0) return 0; 
		var mass2 = {};
		mass2.name = deal;
		mass2.check = false;
		mass2.id = 0;
		mass2.existence = true;
		mass.push(mass2);
		output();
		localStorage.setItem('do',JSON.stringify(mass));               //localStorage поддерживает только строки
	}

	function output(){
		id = 0;
		var out='';
		for (var key in mass){
			id++;
			mass[key].id = id;
			out += (mass[key].check) ? '<input type ="checkbox" class = "checkbox" checked>' + `<s> ${mass[key].name} </s>` + '<a class="remove"> X </a>' : 
										'<input type ="checkbox" class = "uncheckbox" unchecked>' + mass[key].name + '<a class="remove"> X </a>' ;
			if (mass[key].existence) {
				out += '<br>';
			} else {
				out = '';
			}
		}
		document.getElementById('list').innerHTML = out;		
	}
}